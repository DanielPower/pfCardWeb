import urllib.request
import argparse
import sys
import os


def getPage(creature, bestiary):
    url = 'http://paizo.com/pathfinderRPG/prd/'+bestiary+'/'+creature+'.html'

    try:
        response = urllib.request.urlopen(url)
        html = bytes(response.read())
        html = html.decode("utf-8", "strict")
        html = html.replace('\t', '')
        html = html.replace('\n\n', '\n')
        html = html.split('\n')
    except urllib.error.HTTPError as error:
        print("HTTP Error: "+error.reason)
        sys.exit()
    except urllib.error.URLError as error:
        print("URL Error: "+error.reason)

    return html


def getName(titleLine):
    titleLine = titleLine.replace('<b>', '')
    begin = titleLine.find('title">') + 7
    end = titleLine.find('<span')

    return titleLine[begin:end].strip()


def getCreature(args):
    parser = argparse.ArgumentParser(
        description="Get stat blocks from Pathfinder Database"
    )
    parser.add_argument('creature')
    parser.add_argument('-t', '--subType', default=None, required=False)
    parser.add_argument('-b', '--bestiary', default='bestiary', required=False)

    args = parser.parse_args(args)

    page = getPage(args.creature, args.bestiary)
    creatureTypes = []
    for index, line in enumerate(page):
        if 'stat-block-cr' in line:
            typeName = getName(line)
            creatureTypes.append({'line': index, 'name': typeName})

    if not args.subType and len(creatureTypes) > 1:
        print("Please specify a creature type")
        for index, creatureType in enumerate(creatureTypes):
            print(str(index)+' '+creatureType['name'])
        print("A All")
        index = None
        while index is None:
            selection = input('-> ')
            if selection.upper() == 'A':
                for creatureType in creatureTypes:
                    getCreature([args.creature, '--subType='+creatureType['name']])
                sys.exit()
            try:
                index = int(selection)
            except ValueError:
                print("Error: Invalid selection")
        args.subType = creatureTypes[int(index)]['name']

    statBlockIndex = None
    if len(creatureTypes) == 1:
        statBlockIndex = creatureTypes[0]['line']+1
        args.creature = creatureTypes[0]['name']  # Match capitalization
    else:
        for creatureType in creatureTypes:
            if creatureType['name'].upper() == args.subType.upper():
                args.subType = creatureType['name']  # Match capitalization
                statBlockIndex = creatureType['line']+1
        if not statBlockIndex:
            print("sub-creature not found")
            sys.exit()

    statBlock = []

    statBlock.append(page[statBlockIndex-1])
    statBlock.append(page[statBlockIndex-2])

    inSection = False
    for line in page[statBlockIndex:]:
        if 'stat-block-breaker' in line:
            if inSection:
                statBlock.append('</div>')
            statBlock.append('<div class="dontbreak">')
            inSection = True
        if line[0:3] != '<p ':
            if inSection:
                inSection = False
                statBlock.append('</div>')
        if line[0:2] != '<p':
            break
        statBlock.append(line)

    output = ""
    for line in statBlock:
        output += line + '\n'

    return output


if __name__ == '__main__':
    getCreature(sys.argv[1:])

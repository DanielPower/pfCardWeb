from flask import render_template
from app import getCreature
from app import app
from app.forms import CreatureForm


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    title = "Pathfinder Stat Card Generator"
    creature = None
    form = CreatureForm()
    if form.validate_on_submit():
        creature = getCreature.getCreature([form.creature.data])
    return render_template('index.html', title=title, data=creature, form=form)

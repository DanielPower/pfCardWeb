from flask import Flask

app = Flask(__name__)
app.config['SECRET_KEY'] = 'you-will-never-guess'
app.config['CSRF_TOKEN'] = 'something'

from app import routes
from app import getCreature
